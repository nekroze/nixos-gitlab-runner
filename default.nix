{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.modules.gitlab-runner;
  mkRunnerService = executor: {
    name = "gitlab-runner-${executor}";
    value =  let
      name = cfg.name;
      runnerName = "${name}-${executor}";
      config = "/etc/gitlab-runner/${runnerName}.toml";
      gitlab-runner = import ./package.nix {pkgs=pkgs;};
      tags = flatten([
        [ "nixos" name ]
        cfg.tags
        (if (executor == "docker") then "container" else executor)
        (optional (executor == "docker-machine") "container")
        (optional (hasAttr executor cfg.executorTags) (getAttr executor cfg.executorTags))
      ]);
    in rec {
      description = "Gitlab Runner - ${runnerName}";
      after = [ "network-online.target" ] ++ optional (executor == "docker") "docker.service" ++ optional (executor == "docker-machine") "libvirtd.service";
      requires = optional (executor == "docker") "docker.service";
      wantedBy = [ "multi-user.target" ];
      preStart = let
        optIfPath = opt: path: optionalString (!isNull path) "${opt} ${path}";
        registerOptions = [
          "-c ${config}" "--executor ${if executor == "docker-machine" then "docker+machine" else executor}" "--tag-list ${concatStringsSep "," tags}" "-r ${cfg.token}" "--name ${runnerName}" "--url ${cfg.ciUrl}"
          "--tls-ca-file /etc/ssl/certs/ca-certificates.crt"
        ] ++ optionals (executor != "docker-machine") [
          "--env GIT_SSL_CAINFO=/etc/ssl/certs/ca-certificates.crt" "--env SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt"
        ] ++ optionals (executor == "docker" || executor == "docker-machine") [
          "--docker-image ${cfg.dockerImage}" "--docker-volumes /etc/ssl/certs:/etc/ssl/certs:ro"
        ] ++ optionals (executor == "kubernetes") [
          "--kubernetes-image ${cfg.dockerImage}" "--kubernetes-host ${cfg.kubeHost}" "--kubernetes-namespace ${cfg.kubeNamespace}" "--kubernetes-namespace_overwrite_allowed '${cfg.kubeNamespacesAllowed}'"
          (optIfPath "--kubernetes-ca-file" cfg.kubeTlsCa)
          (optIfPath "--kubernetes-cert-file" cfg.kubeTlsCert)
          (optIfPath "--kubernetes-key-file" cfg.kubeTlsKey)
        ] ++ optionals (executor == "docker-machine") [
          "--machine-machine-driver kvm" "--machine-machine-name \"${runnerName}-%s\""
          "--machine-idle-nodes ${toString cfg.machineIdleNodes}" "--machine-idle-time ${toString cfg.machineIdleTime}" "--machine-max-builds ${toString cfg.machineMaxBuilds}"
        ] ++ (optional ((executor == "docker-machine") && !isNull cfg.machineRegistryMirror) "--machine-machine-options engine-registry-mirror=${cfg.machineRegistryMirror}")
        ++ optional ((!isNull cfg.untaggedExecutor) && cfg.untaggedExecutor == executor) "--run-untagged";
      in ''
        ${optionalString (executor == "shell") "HOME=/root ${pkgs.nix}/bin/nix-channel --update"}
        if [ ! -f ${config} ]; then
          if [ -f /var/lib/gitlab-runner/${runnerName}.toml ]; then
            mkdir -p /etc/gitlab-runner
            cp /var/lib/gitlab-runner/${runnerName}.toml ${config}
          else
            ${gitlab-runner.bin}/bin/gitlab-runner register -n ${concatStringsSep " " registerOptions}
            mkdir -p /var/lib/gitlab-runner
            cp ${config} /var/lib/gitlab-runner/${runnerName}.toml
          fi
        fi
	rm -f /bin/bash || true
	ln -s ${pkgs.bash}/bin/bash /bin/bash
        export PATH=/run/wrappers/bin:/var/setuid-wrappers:${pkgs.bash}/bin:$PATH
      '';
      reloadIfChanged = true;
      reload = ''
        RTOKEN=$(cat ${config} | grep token | cut -d '"' -f 2)
        ${gitlab-runner.bin}/bin/gitlab-runner unregister -c ${config} -u ${cfg.ciUrl} -t $RTOKEN -n ${runnerName}
        rm -f ${config} /var/lib/gitlab-runner/${runnerName}.toml
      '' + preStart;
      script = ''
        export PATH=/run/wrappers/bin:/var/setuid-wrappers:${pkgs.bash}/bin:$PATH
        ${gitlab-runner.bin}/bin/gitlab-runner run -c ${config} --working-directory ${cfg.workDir} --service gitlab-runner ${optionalString (!isNull cfg.user) ''--user ${cfg.user}''}
      '';
      serviceConfig = {
        Restart = "always";
        RestartSec = "5";
      };
    };
  };
in {

  options.modules.gitlab-runner = {
    enable = mkEnableOption "Gitlab Runner";
    ciUrl = mkOption {
      type = types.str;
      default = "https://lab";
      description = "The URL to communicate with gitlab.";
    };
    token = mkOption {
      type = types.str;
      default = "temp";
      description = "Gitlab runner registration token.";
    };
    name = mkOption {
      type = types.str;
      default = "runner";
      description = "Name for this runner.";
    };
    executors = mkOption {
      type = types.listOf (types.enum ["shell" "docker" "docker-machine" "kubernetes"]);
      default = [ "docker" ];
      example = [ "docker" "shell" ];
      description = "List of executors to start runners for.";
    };
    kubeHost = mkOption {
      type = types.str;
      description = "Kubernetes host api to connect to";
    };
    kubeTlsCa = mkOption {
      type = types.nullOr types.path;
      default = null;
      description = "Kubernetes auth CA cert";
    };
    kubeTlsCert = mkOption {
      type = types.nullOr types.path;
      default = null;
      description = "Kubernetes client auth cert";
    };
    kubeTlsKey = mkOption {
      type = types.nullOr types.path;
      default = null;
      description = "Kubernetes client auth key";
    };
    kubeNamespace = mkOption {
      type = types.str;
      default = "gitlab";
      description = "Default kubernetes namespace to use";
    };
    kubeNamespacesAllowed = mkOption {
      type = types.str;
      default = "ci-.*";
      description = "Allowed kubernetes namespaces to overwite described as a regex";
    };
    dockerImage = mkOption {
      type = types.str;
      default = "alpine:latest";
      description = "Default docker image for the runner to use under the docker executor.";
    };
    machineRegistryMirror = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "Docker proxy/mirror registry for docker machine usage.";
    };
    machineIdleNodes = mkOption {
      type = types.int;
      default = 0;
      description = "Docker machine maximum idle machines";
    };
    machineIdleTime = mkOption {
      type = types.int;
      default = 0;
      description = "Docker machine maximum time to leave a machine idle before destroying it.";
    };
    machineMaxBuilds = mkOption {
      type = types.int;
      default = 0;
      description = "Docker machine maximum number of builds processed by a single machine before destroying it.";
    };
    user = mkOption {
      type = types.nullOr types.str;
      default = "gitlab-runner";
      description = "User to execute shell runs as, if null root is used and if this is `gitlab-runner` that user will be automatically created.";
    };
    userUID = mkOption {
      type = types.int;
      default = 257;
      description = "User ID for the gitlab-runner user if the `user` option is set to `gitlab-runner`";
    };
    userGID = mkOption {
      type = types.int;
      default = 257;
      description = "Group ID for the gitlab-runner user if the `user` option is set to `gitlab-runner`";
    };
    dockerGc = mkOption {
      type = types.bool;
      default = true;
      description = "Enable the docker garbage collector.";
    };
    untaggedExecutor = mkOption {
      type = types.nullOr types.str;
      default = "docker";
      description = "The executor that should be registered for untagged builds or null for none.";
    };
    groups = mkOption {
      type = types.listOf types.str;
      default = [ "docker" ];
      description = "List of extra groups the shell runner should have access to.";
    };
    tags = mkOption {
      type = types.listOf types.str;
      default = [];
      description = "List of tags to add to all runners on this machine.";
    };
    executorTags = mkOption {
      type = types.attrs;
      default = {};
      description = "Attribute set of tags to add to this machines runners. Attribute names will scope the tags to specific executors";
    };
    workDir = mkOption {
      default = "/var/lib/gitlab-runner";
      type = types.path;
      description = "The working directory to be used.";
    };
  };

  config = mkIf cfg.enable (mkMerge [
    {
      systemd.services = builtins.listToAttrs (map mkRunnerService cfg.executors);
      security.pki.certificateFiles = [ "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt" ];
    }
    (mkIf (builtins.any (e: e == "docker") cfg.executors) {
      virtualisation.docker.enable = true;
      systemd.services.dockergc = rec {
         requires = ["docker.service"];
         after = requires;
         startAt = "weekly";
         serviceConfig = {
           Type = "oneshot";
           ExecStart = "${pkgs.docker-gc}/bin/docker-gc";
         };
       };
    })
    (mkIf (builtins.any (e: e == "shell") cfg.executors) {
      nix.gc.automatic = true;
      environment.systemPackages = with pkgs; [
        gitMinimal
        bash
      ];
    })
    (mkIf ((builtins.any (e: e == "shell" || e == "kubernetes") cfg.executors) && cfg.user == "gitlab-runner") {
      users.extraUsers.gitlab-runner = {
        group = "gitlab-runner";
        extraGroups = cfg.groups;
        uid = cfg.userUID;
        home = cfg.workDir;
        createHome = true;
      };
      users.extraGroups.gitlab-runner = {
        gid = cfg.userGID;
      };
    })
    (mkIf (builtins.any (e: e == "docker-machine") cfg.executors) {
      hardware.enableKSM = true;
      virtualisation.libvirtd.enable = true;
      environment.systemPackages = with pkgs; [
        libvirt
        docker-machine
        docker-machine-kvm
      ];
    })
  ]);
}

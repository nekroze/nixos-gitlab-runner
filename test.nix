import <nixpkgs/nixos/tests/make-test.nix> {
  machine = { config, pkgs, ... }: {
    imports = [
      ./default.nix
    ];
    modules.gitlab-runner = {
      enable = true;
      executors = [ "shell" ];
    };
  };

  testScript = ''
  '';
}
